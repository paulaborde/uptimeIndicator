#!/usr/bin/python

import objc
import rumps
from uptime import uptime
import os
import time

class AwesomeStatusBarApp(rumps.App):
    @rumps.clicked("Preferences")
    def prefs(self, _):
        rumps.alert("jk! no preferences available!")

if __name__ == "__main__":
    while True:
        uptime=uptime()
        jours=int(uptime//(3600*24))
        heures=int(uptime%(3600*24)/3600)
        uptime_str= str(jours) + " jours et " + str(heures) +" heures"
        AwesomeStatusBarApp(uptime_str).run()
        time.sleep(3600)
